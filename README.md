## ARU_FACE

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ARU_FACE) [GitOsc](http://git.oschina.net/nzsghdx/aru_face) [Coding](https://coding.net/u/nzsghdx/p/ARU_FACE/git) [Bitbucket](https://bitbucket.org/nzsghdx/aru_face)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

***本仓库包括:***

- ***[阿鲁](#in)***
- ***[Telegram表情](#tg)***
- ***[WordPress表情插件](#wp)***
- ***[QQ表情](#qq)***
- ***[. . .](#up)***

---


### <span id="in">阿鲁</span>

***是我也是很多人都喜欢的一套表情包,所以打算做成各种各样的表情包.***

***表情原作者[新浪微博](http://weibo.com/silenthiker)***

***最新表情包地址[百度云盘](https://pan.baidu.com/s/1i32gtEX#list/path=%2F)***

***不同的表情在不同的分支里.***

---

---


### <span id="tg">Telegram表情</span>

***由于一个贴图包只能最多120个贴图,所以我打算分为几种类型精华版什么什么的.现在他是这样子的:[TGT](https://t.me/addstickers/nzsghdx).***

---


---


### <span id="wp">WordPress表情插件</span>

***打算做成一个插件,下载并更新之后自动替换原有的评论表情.还可以扩展下实现快捷替换其他表情的插件.等我慢慢搞吧.***


---


---


### <span id="qq">QQ表情</span>

***原作者分享的百度网盘里有...***  

***qq分支里有一份.***

---

---


### <span id="up">更多更新中...</span>

***更多表情支持中...***

---


>***联系我***

>***[GitHub](http://github.com/nzsghdx)***

>***[GitOsc](http://git.oschina.net/nzsghdx)***

>***[Telegram](https://t.me/nzsghdx)***

>***[网站](http://nzsghdx.com)***

>***[邮箱](mailto:dalao@nzsghdx.com)***

><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=2846588290&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:2846588290:51" alt="" title=""/></a>